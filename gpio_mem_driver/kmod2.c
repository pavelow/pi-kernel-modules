#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/device.h>
#include <linux/io.h>

MODULE_LICENSE("GPL");              ///< The license type -- this affects runtime behavior
MODULE_AUTHOR("Daniel McCarthy");      ///< The author -- visible when you use modinfo
MODULE_DESCRIPTION("A simple kernel module.");  ///< The description -- see modinfo
MODULE_VERSION("0.1");

static char *name = "world"; //LKM argument
module_param(name, charp, S_IRUGO); //Char pointer, read only (RUserGroupOther)
MODULE_PARM_DESC(name, "Name parameter"); //Displayed in kern.log

static void __iomem* gpio_ptr;

// GPIO Register Mapping
#define GPIO_PHYS 0x7e200000 // GPIO physical memory location
#define GPIO_SIZE 0x9F // GPIO map size

#define GPFSEL0_OFFSET 0x00   //Function Select Register offset
#define GPSET0_OFFSET 0x1c    //Pin Output Set Register offset
#define GPCLR0_OFFSET 0x28    //Pin Output Clear Register offset
#define GPLEV0_OFFSET 0x34    //Pin Level Register offset
#define GPLEV1_OFFSET 0x38    //Pin Level Register offset
#define GPPUD_OFFSET 0x94     //Pull-up/down Enable
#define GPPUDCLK0_OFFSET 0x98 //Pu/Pd Enable Clock

// GPIO Bit Mapping
#define FSEL_I 0x00000000 // Input
#define FSEL_O 0x00000001 // Output << GPIOn*3
#define GPSET 0x00000001 // << GPIOn


static int __init hello_init(void) {
	// Get GPIO MMIO Allocation
	if( request_mem_region(GPIO_PHYS, GPIO_SIZE, "GPIO") == NULL ) {
		printk(KERN_ALERT "kmod: could not request memory 0x%08x\n", GPIO_PHYS);
		return -EBUSY;
	};

	// Map GPIO MMIO to Kernel
	gpio_ptr = (u32*)ioremap(GPIO_PHYS, GPIO_SIZE); //Map Memory Region into Kernel
	if(gpio_ptr == NULL) {
		printk(KERN_ALERT "kmod: ioremap failed!");
		return -EIO;
	}
	printk(KERN_INFO "kmod: ioremap successful!\n");


	printk(KERN_INFO "kmod: GPLEV0: 0x%08x\n", ioread32(gpio_ptr + GPLEV0_OFFSET)); //Read GPLEV0
	printk(KERN_INFO "kmod: GPLEV1: 0x%08x\n", ioread32(gpio_ptr + GPLEV1_OFFSET)); //ead GPLEV1
	printk(KERN_INFO "kmod: GPFSEL0: 0x%08x\n", ioread32(gpio_ptr + GPFSEL0_OFFSET)); //ead GPLEV1

	iowrite32( 100<<0, gpio_ptr + GPFSEL0_OFFSET); //Set GPIO0 as Output
	iowrite32( 1<<0, gpio_ptr + GPSET0_OFFSET); //Set GPIO0 High

	printk(KERN_INFO "kmod: GPFSEL0: 0x%08x\n", ioread32(gpio_ptr + GPFSEL0_OFFSET)); //ead GPLEV1
	printk(KERN_INFO "kmod: GPLEV0: 0x%08x\n", ioread32(gpio_ptr + GPLEV0_OFFSET)); //Read GPLEV0
	printk(KERN_INFO "kmod: GPLEV1: 0x%08x\n", ioread32(gpio_ptr + GPLEV1_OFFSET)); //ead GPLEV1


	return 0;
}

static void __exit hello_exit(void) {
	iounmap(gpio_ptr); // Unmap memory
	release_mem_region(GPIO_PHYS, GPIO_SIZE); //Release Memory Region
	printk(KERN_INFO "kmod: Byeeee!\n");
}

module_init(hello_init);
module_exit(hello_exit);
