//#include <linux/init.h>
//#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/kthread.h>
#include <linux/delay.h>
#include <linux/io.h>
#include <linux/device.h> //mmio operations

MODULE_LICENSE("GPL");              ///< The license type -- this affects runtime behavior
MODULE_AUTHOR("Daniel McCarthy");      ///< The author -- visible when you use modinfo
MODULE_DESCRIPTION("A simple gpio module.");  ///< The description -- see modinfo
MODULE_VERSION("0.1");

static char *name = "world"; //LKM argument
module_param(name, charp, S_IRUGO); //Char pointer, read only (RUserGroupOther)
MODULE_PARM_DESC(name, "Name parameter"); //Displayed in kern.log

// GPIO Register Mapping
#define GPIO_PHYS 0x7e200000 // GPIO physical memory location -- Possibly from the GPU Address space
#define GPIO_CPUPHYS 0x20200000 // Address from CPU address space :^)
#define GPIO_SIZE 0xB3 // GPIO map size

#define GPFSEL0_OFFSET 0x00   //Function Select Register offset
#define GPSET0_OFFSET 0x1c    //Pin Output Set Register offset
#define GPCLR0_OFFSET 0x28    //Pin Output Clear Register offset
#define GPLEV0_OFFSET 0x34    //Pin Level Register offset
#define GPLEV1_OFFSET 0x38    //Pin Level Register offset
#define GPPUD_OFFSET 0x94     //Pull-up/down Enable
#define GPPUDCLK0_OFFSET 0x98 //Pu/Pd Enable Clock

#define GPIO_N 4
struct task_struct *pulse; // kthread handle
static void __iomem* gpio_ptr; // pointer to gpio mmio


static int pulseThread(void *data) {
	for(;;) {
		iowrite32( 1<<GPIO_N, gpio_ptr + GPSET0_OFFSET ); //Set GPIO
		usleep_range(2000,2000); // usleep_range, inaccurate but light vs udelay which uses busy wait
		iowrite32( 1<<GPIO_N, gpio_ptr + GPCLR0_OFFSET ); //Clear GPIO
		usleep_range(2000,2000);
		if (kthread_should_stop()) break; //Make sure thread can actually exit
	}

	return 0;
}

static int __init gpio_init(void) {
	u32 GPFSEL0_m; //Declare at the top becasue of gcc -pedantic

	printk(KERN_INFO "kmod: Hello!\n");

	// Reserve GPIO MMIO
	// if(!request_mem_region(GPIO_CPUPHYS, GPIO_SIZE, "GPIO")) {
	// 	printk(KERN_ALERT "kmod: could not request memory 0x%08p\n", GPIO_PHYS);
	// 	return -ENODEV;
	// };

	// Note ^
	// Something in the kernel already has this memory range, probably gpio driver.
	// I am ignoring that and mapping it anyway -- bad practise

	// Map GPIO MMIO to Kernel
	gpio_ptr = ioremap(GPIO_CPUPHYS, GPIO_SIZE); //Map Memory Region into Kernel
	if(gpio_ptr == NULL) {
		printk(KERN_ALERT "kmod: ioremap failed!");
		return -EIO;
	}
	printk(KERN_INFO "kmod: ioremap successful @%p\n", gpio_ptr);
	GPFSEL0_m = ioread32(gpio_ptr + GPFSEL0_OFFSET) & (0xFFFFFFFF ^ 0b111 << 3); // Read register and clear bits I want to change
	GPFSEL0_m |= 1<<(GPIO_N*3); // Write bits I want (Set GPIO_N as output)
	iowrite32( GPFSEL0_m, gpio_ptr + GPFSEL0_OFFSET ); //Write modified bits to register

	printk(KERN_INFO "kmod: Set GPIO%d as Output\n", GPIO_N);

	// Start KThread
	pulse = kthread_run(pulseThread, NULL, "Pulser");
	printk(KERN_INFO "kmod: started pulser thread!\n");

	return 0;
}



static void __exit gpio_exit(void) {
	kthread_stop(pulse);
	printk(KERN_INFO "kmod: stopped pulser thread!\n");
	iounmap(gpio_ptr); // Unmap memory
	release_mem_region(GPIO_CPUPHYS, GPIO_SIZE); //Release Memory Region
	printk(KERN_INFO "kmod: Byeeee!\n");
}

module_init(gpio_init);
module_exit(gpio_exit);
