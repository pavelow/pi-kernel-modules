//#include <linux/init.h>
//#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/gpio.h>
#include <linux/kthread.h>
#include <linux/delay.h>

MODULE_LICENSE("GPL");              ///< The license type -- this affects runtime behavior
MODULE_AUTHOR("Daniel McCarthy");      ///< The author -- visible when you use modinfo
MODULE_DESCRIPTION("A simple gpio module.");  ///< The description -- see modinfo
MODULE_VERSION("0.1");

static char *name = "world"; //LKM argument
module_param(name, charp, S_IRUGO); //Char pointer, read only (RUserGroupOther)
MODULE_PARM_DESC(name, "Name parameter"); //Displayed in kern.log

#define GPIO_N 4
struct task_struct *pulse; // kthread handle


static int pulseThread(void *data) {
	for(;;) {
		gpio_set_value(GPIO_N, 1); //On
		usleep_range(2000,2000); // usleep_range, inaccurate but light vs udelay which uses busy wait
		gpio_set_value(GPIO_N, 0); //Off
		usleep_range(2000,2000);
		if (kthread_should_stop()) break; //Make sure thread can actually exit
	}

	return 0;
}

static int __init hello_init(void) {
	printk(KERN_INFO "kmod: Hello!\n");

	if(gpio_request(GPIO_N, "GPIO_N") < 0) {
		printk(KERN_INFO "Can't get GPIO 2\n");
		return -EIO;
	}

	gpio_direction_output(GPIO_N, 0);

	// Start KThread
	pulse = kthread_run(pulseThread, NULL, "Pulser");
	printk(KERN_INFO "kmod: started pulser thread!\n");

	return 0;
}



static void __exit hello_exit(void) {
	kthread_stop(pulse);
	printk(KERN_INFO "kmod: stopped pulser thread!\n");
	gpio_free(GPIO_N);
	printk(KERN_INFO "kmod: Byeeee!\n");
}

module_init(hello_init);
module_exit(hello_exit);
