// For poking the character device for testing

#include<stdio.h>
#include<stdlib.h>
#include<errno.h>
#include<fcntl.h>
#include<string.h>
#include<unistd.h>

int main(int argc, char* argv) {
	int fd = open("/dev/chardev", O_RDWR);
	char str[] = "Hello World";
	char ret[256] = {0};

	write(fd, str, strlen(str));
	printf("Sent: %s\n", str);
	read(fd, ret, 256);
	printf("Got: %s\n", ret);

	return 0;
}
