//device driver includes

#include <linux/init.h>
#include <linux/module.h>
#include <linux/device.h>
#include <linux/kernel.h>
#include <linux/fs.h>
#include <linux/mutex.h>
#include <asm/uaccess.h>
#include <asm/types.h>
#include <linux/io.h>

MODULE_LICENSE("GPL");              ///< The license type -- this affects runtime behavior
MODULE_AUTHOR("Daniel McCarthy");      ///< The author -- visible when you use modinfo
MODULE_DESCRIPTION("A simple kernel module.");  ///< The description -- see modinfo
MODULE_VERSION("0.1");

static int majorNumber;
static char message[256] = {0};
static short size_of_message = 0;
static int numberOpens = 0;
static struct class*  charClass  = NULL; ///< The device-driver class struct pointer
static struct device* charDevice = NULL; ///< The device-driver device struct pointer

static DEFINE_MUTEX(dev_mtx);


#define DEVICE_NAME "chardev"
#define CLASS_NAME "charkmod"


static void __iomem* gpio_ptr;

// GPIO Register Definitions
#define GPIO_PHYS 0x7e200000 // GPIO physical memory location
#define GPIO_SIZE 0xB4 // GPIO map size

#define GPFSEL0_OFFSET 0x00   //Function Select Register offset
#define GPSET0_OFFSET 0x1c    //Pin Output Set Register offset
#define GPCLR0_OFFSET 0x28    //Pin Output Clear Register offset
#define GPLEV0_OFFSET 0x34    //Pin Level Register offset
#define GPLEV1_OFFSET 0x38    //Pin Level Register offset
#define GPPUD_OFFSET 0x94     //Pull-up/down Enable
#define GPPUDCLK0_OFFSET 0x98 //Pu/Pd Enable Clock


#define FSEL_I 0x00000000 // Input
#define FSEL_O 0x00000001 // Output << GPIOn*3

#define GPSETn 0x00000001 // << GPIOn

// The prototype functions for the character driver -- must come before the struct definition
static int     dev_open(struct inode *, struct file *);
static int     dev_release(struct inode *, struct file *);
static ssize_t dev_read(struct file *, char *, size_t, loff_t *);
static ssize_t dev_write(struct file *, const char *, size_t, loff_t *);

static struct file_operations fops =
{
   .open = dev_open,
   .read = dev_read,
   .write = dev_write,
   .release = dev_release,
};

static int __init kmod_init(void) {
        printk(KERN_INFO "kmod: Hello!\n");

	// Get GPIO MMIO 
	if( request_mem_region(GPIO_PHYS, GPIO_SIZE, DEVICE_NAME) == NULL ) { // Allocate GPIO memory
		printk(KERN_ALERT "kmod: could not request memory 0x%08x\n", GPIO_PHYS);
		return -EBUSY;
	};

	gpio_ptr = (u32*)ioremap(GPIO_PHYS, GPIO_SIZE); //Map Memory Region into Kernel
	if(gpio_ptr == NULL) {
		printk(KERN_ALERT "kmod: ioremap failed!");
		return -EIO;
	}
	printk(KERN_INFO "kmod: ioremap successful!\n");
	
	printk(KERN_INFO "kmod: GPLEV0: 0x%08x\n", ioread32(gpio_ptr + GPLEV0_OFFSET)); //Read GPLEV0
	printk(KERN_INFO "kmod: GPLEV1: 0x%08x\n", ioread32(gpio_ptr + GPLEV1_OFFSET)); //ead GPLEV1
	printk(KERN_INFO "kmod: GPFSEL0: 0x%08x\n", ioread32(gpio_ptr + GPFSEL0_OFFSET)); //ead GPLEV1
	
	iowrite32( 0x00000001, gpio_ptr + GPFSEL0_OFFSET); //Set GPIO0 as Output
	iowrite32( 0x00000001, gpio_ptr + GPSET0_OFFSET); //Set GPIO0 High
	
	printk(KERN_INFO "kmod: GPFSEL0: 0x%08x\n", ioread32(gpio_ptr + GPFSEL0_OFFSET)); //ead GPLEV1
	printk(KERN_INFO "kmod: GPLEV0: 0x%08x\n", ioread32(gpio_ptr + GPLEV0_OFFSET)); //Read GPLEV0
	printk(KERN_INFO "kmod: GPLEV1: 0x%08x\n", ioread32(gpio_ptr + GPLEV1_OFFSET)); //ead GPLEV1

	mutex_init(&dev_mtx);

	// Get Major Number
	majorNumber = register_chrdev(0, DEVICE_NAME, &fops);
	if( majorNumber < 0) {
		printk(KERN_INFO "kmod: failed to register a major number\n");
		return majorNumber;
	}
	printk(KERN_INFO "kmod: Registered correctly with Major Number  %d\n", majorNumber);

	// Register Device Class
	charClass = class_create(THIS_MODULE, CLASS_NAME);
	if(IS_ERR(charClass)) {
		unregister_chrdev(majorNumber, DEVICE_NAME);
		printk(KERN_INFO "kmod: failed to register device class\n");
		return PTR_ERR(charClass);
	}
	printk(KERN_INFO "kmod: device class created!\n");

	// Register device driver
	charDevice = device_create(charClass, NULL, MKDEV(majorNumber, 0), NULL, DEVICE_NAME);
	if (IS_ERR(charDevice)) {
		class_destroy(charClass);
		unregister_chrdev(majorNumber, DEVICE_NAME);
		printk(KERN_INFO "kmod: Failed to create the device :(");
		return PTR_ERR(charDevice);
	}
	printk(KERN_INFO "kmod: Device Created!");

	return 0;
}

static void __exit kmod_exit(void) {
	device_destroy(charClass, MKDEV(majorNumber, 0));
	class_unregister(charClass);
	class_destroy(charClass);
	unregister_chrdev(majorNumber, DEVICE_NAME);
	mutex_destroy(&dev_mtx);
	
	iounmap(gpio_ptr); //unmap memory
	release_mem_region(GPIO_PHYS, GPIO_SIZE); //Release Memory Region

        printk(KERN_INFO "kmod: Byeeee!\n");
}

// Called when device is opened
static int dev_open(struct inode *inodep, struct file *filep) { //inodep and filep are pointers to inode and file objects respectively
	if(!mutex_trylock(&dev_mtx)) {
		printk(KERN_INFO "kmod: Already in use, cannot open.");
		return -EBUSY;
	}
	numberOpens++;
	pr_info("kmod: %s has been opend %d times\n", DEVICE_NAME, numberOpens);
	return 0;
}

// Called when device is read from
static ssize_t dev_read(struct file *filep, char *buffer, size_t len, loff_t *offset) {

	int error_count = 0;
	error_count = copy_to_user(buffer, message, size_of_message);

	if (error_count==0){            // if true then have success
		printk(KERN_INFO "kmod: Sent %d characters to the user\n", size_of_message);
		return (size_of_message=0);  // clear the position to the start and return 0
	}
	else {
		printk(KERN_INFO "kmod: Failed to send %d characters to the user\n", error_count);
		return -EFAULT;              // Failed -- return a bad address message (i.e. -14)
	}
}

// Called when device is written to
static ssize_t dev_write(struct file *filep, const char *buffer, size_t len, loff_t *offset) {
	static char userBuffer[256] = {0};
	if(copy_from_user(userBuffer, buffer, len) != 0) {  // Must use this to copy from userspace, can't dereference directly
		printk(KERN_INFO "kmod: Copy from user failed");
	}
	sprintf(message, "%s(%zu letters)\n", userBuffer, len); // I get an error when trying to acces buffer, no idea why yet
	size_of_message = strlen(message);
	printk(KERN_INFO "kmod: Rx'd %zu chars from the user: %s\n", len, message);
	return len;
}

static int dev_release(struct inode *inodep, struct file *filep) {
	mutex_unlock(&dev_mtx);
	printk(KERN_INFO "kmod: Device closed");
	return 0;
}


module_init(kmod_init);
module_exit(kmod_exit);
