#include <linux/init.h>
#include <linux/module.h>
#include <asm/io.h>
#include <linux/timer.h>
#include <linux/device.h>
#include <linux/err.h>
#include <linux/mm.h>


MODULE_LICENSE("GPL");              ///< The license type -- this affects runtime behavior
MODULE_AUTHOR("Daniel McCarthy");      ///< The author -- visible when you use modinfo
MODULE_DESCRIPTION("A simple kernel module.");  ///< The description -- see modinfo
MODULE_VERSION("0.1");

static char *name = "world"; //LKM argument
module_param(name, charp, S_IRUGO); //Char pointer, read only
MODULE_PARM_DESC(name, "Name parameter"); //Displayed in kern.log

static struct class *s_pDeviceClass;
static struct device *s_pDeviceObject;

static ssize_t set_value_callback(struct device* dev, struct device_attribute* attr, const char* buf, size_t count) {
	long period_value = 0;

	if (kstrtol(buf, 10, &period_value) < 0) { // If we didn't suceed in converting to a long
		return -EINVAL; // Invalid argument
	}

	pr_info("kmod: %d\n", period_value);
	return count;
}

static DEVICE_ATTR(value, S_IRWXU | S_IRWXG, NULL, set_value_callback); // This is where dev_attr_value is defined

static int __init hello_init(void) {
        int result;

        pr_info("kmod: Hello %s\n", name);

        s_pDeviceClass = class_create(THIS_MODULE, "LedBlink");
        BUG_ON(IS_ERR(s_pDeviceClass));

        s_pDeviceObject = device_create(s_pDeviceClass, NULL, 0, NULL, "LedBlink");
        BUG_ON(IS_ERR(s_pDeviceObject));

        result = device_create_file(s_pDeviceObject, &dev_attr_value);
        BUG_ON(result < 0);

        return 0;
}

static void __exit hello_exit(void) {
        pr_info("kmod: Byeeee!\n");
        device_remove_file(s_pDeviceObject, &dev_attr_value);
        device_destroy(s_pDeviceClass, 0);
        class_destroy(s_pDeviceClass);
}



module_init(hello_init);
module_exit(hello_exit);
