#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>

MODULE_LICENSE("GPL");              ///< The license type -- this affects runtime behavior
MODULE_AUTHOR("Daniel McCarthy");      ///< The author -- visible when you use modinfo
MODULE_DESCRIPTION("A simple kernel module.");  ///< The description -- see modinfo
MODULE_VERSION("0.1");

static char *name = "world"; //LKM argument
module_param(name, charp, S_IRUGO); //Char pointer, read only
MODULE_PARM_DESC(name, "Name parameter"); //Displayed in kern.log

static int __init hello_init(void) {
        pr_info("kmod: Hello %s\n", name);
        return 0;
}

static void __exit hello_exit(void) {
        pr_info("kmod: Byeeee!\n");
}

module_init(hello_init);
module_exit(hello_exit);
